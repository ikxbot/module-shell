<?php
namespace Ikx\Shell\Command;

use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;

class PhpCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public $threaded = true;

    public function describe()
    {
        return "Run a PHP script in a sandbox";
    }

    public function run() {
        if (count($this->params)) {
            $script = implode(' ', $this->params);
            $file = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'script-' . $this->nickname . '.ikx.php';
            file_put_contents($file, '<?php ' . $script);
            $response = trim(shell_exec('php ' . $file . ' 2>&1'));
            unlink($file);

            $lines = explode(PHP_EOL, $response);

            $this->msg($this->channel, Format::bold($script));
            foreach($lines as $line) {
                $this->msg($this->channel, $line);
            }
        } else {
            $this->msg($this->channel, sprintf('%s: %s command expects at least one parameter, none given',
                Format::bold('ERROR'), $this->command));
        }
    }
}