<?php
namespace Ikx\Shell\Command;

use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;

class ShellCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public $threaded = true;

    public function describe()
    {
        return "Run a shell command on the server";
    }

    public function run() {
        if (count($this->params)) {
            $shellCommand = implode(' ', $this->params) . ' 2>&1';
            ob_start();
            $response = trim(shell_exec($shellCommand));
            $buffer = ob_get_contents();
            ob_end_clean();
            $response .= PHP_EOL . $buffer;
            $lines = explode(PHP_EOL, $response);

            $this->msg($this->channel, Format::bold($shellCommand));
            foreach($lines as $line) {
                $this->msg($this->channel, $line);
            }
        } else {
            $this->msg($this->channel, sprintf('%s: %s command expects at least one parameter, none given',
                Format::bold('ERROR'), $this->command));
        }
    }
}